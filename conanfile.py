from conans import ConanFile, CMake, tools
import os

class MathsConan(ConanFile):
    name = "Maths"
    version = "1.0"
    license = "<Put the package license here>"
    author = "Malen miztamalen@gmail.com"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "Maths"
    topics = ("1.0", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    #exports_sources = "src/*"
    
    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")
        cmake.build()

    def package(self):
        #self.cmake.install()
        self.copy("*.h", dst="include", src="src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", src= "lib")
        self.copy("*.exe", dst="bin", src="bin")

    def package_info(self):
        #super(MathsConan, self).package_info()
        self.cpp_info.libs = ["Maths"]
        #self.cpp_info.name = "Windows"
        #self.cpp_info.names["cmake"] = "Windows"

        
